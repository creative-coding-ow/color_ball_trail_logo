import java.awt.Color;

class Ball{
  // Seed
  long seed;
  // Größenbegrenzung für den Ball
  float maxBallSize = height / 2;
  float minBallSize = height / 10;
  // Tracker und dessen maximaler wert zur Darstellung von Farben
  float track,max; // Input für Farbdarstellung
  
  Ball(long seed){
    this.seed = seed;
  }
  
  void draw(){
    // Zum verändern des Seeders
    int seedCounter = 0;
    
    // Noise-Value für Größe und Position
    noiseSeed(seed+seedCounter++);
    float w = minBallSize+noise(xoff)*(maxBallSize-minBallSize);
    noiseSeed(seed+seedCounter++);
    float h = minBallSize+noise(xoff)*(maxBallSize-minBallSize);
    noiseSeed(seed+seedCounter++);
    float x = noise(xoff)*height;
    noiseSeed(seed+seedCounter++);
    float y = noise(xoff)*width;
    
    // Was genau bewirkt das hier?
    xoff += xincrement;
    
    // Tracker auf Farbspecktrum mappen
    float fillColor = map(track,0,max,0,1);
    Color c = Color.getHSBColor(fillColor, 1.0, 1.0);
    
    // Style festlegen
    fill(c.getRed(),c.getGreen(),c.getBlue());
    noStroke();
    
    // Objekt malen
    ellipse(x,y,w,h);
  }
  
  void setCalcColorParams(float track, float max){
    this.track = track;
    this.max = max;
  }
}
