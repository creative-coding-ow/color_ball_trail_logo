# Color Ball Trail Logo

## Describtion

User data are used as seeds, similar to GitHub, to create a unique (profile-) picture.

## Rules

1. Randomness is calculated with user seed (WIP)
1. The size and position of a sphere should change randomly
1. A sphere is supposed to leave a trace behind it
1. The trail fades over time

## Interaction

- Left click: Save snapshot
- Scroll down: Intensify darkening
- Scroll up: Diminish darkening
- KeyPress("r"): Recording frames

## Used Methods

- Noise (seeded)
- Map(mouse position -> color)
- Recording Mode (Press "r")

![Animation](animation.gif)
