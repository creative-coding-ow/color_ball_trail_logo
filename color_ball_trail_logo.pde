/**
 * topic: generative logo design -> Profilbilder
 * concept: 
 * - Userdaten werden als Seed genutzt ähnlich wie bei GitHub
 * - Kugeln sollen in ihrer Größe und Position variieren und eine verblassende Spur hinter sich herziehen
 * interaction:
 * - Der User soll auf das Bild klicken können um den aktuellen Zustand zu speichern
 * - Die Position der Maus gibt die Farbe der Beiden Kreise an Spectrum(mouseX) bzw Spectrum(mouseY)
 * - Das Mausrad kann die Geschwindigkeit des Abdunkelns ändern 
 */

Ball b1, b2;
float xoff = 0.00;
float xincrement = 0.002; 
float w=0,h=0;
// Zum Abdunkeln
float transparenz = 2;

void setup(){
  size(500,500);
  background(0);
  b1 = new Ball(System.currentTimeMillis());
  b2 = new Ball(System.currentTimeMillis()/2);
}

void draw(){
  abdunkeln();
  b1.setCalcColorParams(mouseX,width);
  b2.setCalcColorParams(mouseY,height);
  b1.draw();
  b2.draw();
  // Record frames if needed
  recording();
}
boolean recording = false;
void recording(){
  if(recording){
    if(frameCount%4 == 0){
      saveFrame("images/image####.png");
    }
  }
}

// Toggle recording
void keyPressed(){
  if(key == 'r' || key == 'R'){
    recording = !recording;
  }
}

void mouseClicked() {
  print("save picture ######");
  saveFrame("snapshot/picture-######.png");
}

void mouseWheel(MouseEvent event) {
  transparenz += event.getCount();
}


void abdunkeln(){
  fill(0, transparenz);
  rect(0,0,width,height);
}
